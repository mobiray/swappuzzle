﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class tmpScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    private float screenWidth = 1080f;
    private int camWidth;
    private Vector2 initPosition;
    private int prevSibling;

    void Start()
    {
        camWidth = Camera.main.pixelWidth;
        initPosition = this.GetComponent<RectTransform>().anchoredPosition;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        DOTween.Kill(this);
        prevSibling = this.transform.GetSiblingIndex();
        this.transform.SetAsLastSibling();
        Debug.Log((Camera.main.WorldToScreenPoint(this.transform.position) * 1080f / camWidth));
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.GetComponent<RectTransform>().anchoredPosition += eventData.delta * 1080f / camWidth;
      //  Debug.Log("current position = " + eventData.position + ", true position = " + eventData.position * 1080f / camWidth);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.GetComponent<RectTransform>().DOAnchorPos(initPosition, 0.2f).SetEase(Ease.Linear);
        //GraphicRaycaster gr = this.GetComponent<GraphicRaycaster>();
        ////Create the PointerEventData with null for the EventSystem
        //List<RaycastResult> results = new List<RaycastResult>();
        ////Raycast it
        //gr.Raycast(eventData, results);
        //Debug.Log("raycast = " + eventData.pointerCurrentRaycast.gameObject.name);
        
    }

    public void SettingSibling()
    {
        this.transform.SetSiblingIndex(prevSibling);
    }
}
